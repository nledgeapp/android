package conference.edge.netlight.netlightedgeconference;


import android.util.Log;

/**
 * Created by markus on 16-03-28.
 */
public class NLog {
    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.d(name, message);
        }
    }

    public static void d(String message, Object ...args) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.d(name, String.format(message, args));
        }
    }

    public static void i(String message) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.i(name, message);
        }
    }

    public static void i(String message, Object ...args) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.i(name, String.format(message, args));
        }
    }

    public static void i(String message, Throwable e) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.i(name, message, e);
        }
    }

    public static void i(String message, Throwable e, Object ...args) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.i(name, String.format(message, args), e);
        }
    }

    public static void e(String message) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.e(name, message);
        }
    }

    public static void e(String message, Object ...args) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.e(name, String.format(message, args));
        }
    }

    public static void e(String message, Throwable e) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.e(name, message, e);
        }
    }

    public static void e(String message, Throwable e, Object ...args) {
        if (BuildConfig.DEBUG) {
            String name = getClassNameMethodAndLineNumber();
            Log.e(name, String.format(message, args), e);
        }
    }

    private static String getClassNameMethodAndLineNumber() {
        String fullClassName = Thread.currentThread().getStackTrace()[4].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[4].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[4].getLineNumber();
        return className + "." + methodName + "():" + lineNumber;
    }
}
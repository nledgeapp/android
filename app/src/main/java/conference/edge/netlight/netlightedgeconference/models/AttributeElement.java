package conference.edge.netlight.netlightedgeconference.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by markus on 16-05-16.
 */
public class AttributeElement {
    private String name;
    private List<Attribute> attributes;

    public AttributeElement(String name, List<Attribute> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public List<Attribute> getAttributes() {
        if(attributes == null) {
            attributes = new ArrayList<>();
        }
        return attributes;
    }
}

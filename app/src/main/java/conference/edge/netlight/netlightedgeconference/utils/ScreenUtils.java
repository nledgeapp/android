package conference.edge.netlight.netlightedgeconference.utils;

import android.content.Context;

/**
 * Created by markus on 16-04-11.
 */
public class ScreenUtils {
    public static int dpToPx(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dp * scale);
    }
}

package conference.edge.netlight.netlightedgeconference.utils;

import android.view.View;
import android.view.animation.AlphaAnimation;

/**
 * Created by markus on 16-04-11.
 */
public class AnimationUtils {
    private static final long DEFAULT_FADE_DURATION = 200;

    public static void fadeOut(View view) {
        fadeOut(view, DEFAULT_FADE_DURATION);
    }

    public static void fadeOut(View view, long duration) {
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(duration);
        view.startAnimation(anim);
    }

    public static void fadeIn(View view) {
        fadeIn(view, DEFAULT_FADE_DURATION);
    }

    public static void fadeIn(View view, long duration) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(duration);
        view.startAnimation(anim);
    }


}

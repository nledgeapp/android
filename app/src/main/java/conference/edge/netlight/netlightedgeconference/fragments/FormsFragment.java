package conference.edge.netlight.netlightedgeconference.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.adapters.FormsAdapter;
import conference.edge.netlight.netlightedgeconference.adapters.SearchAdapter;
import conference.edge.netlight.netlightedgeconference.models.Form;
import conference.edge.netlight.netlightedgeconference.models.Person;
import conference.edge.netlight.netlightedgeconference.models.User;
import conference.edge.netlight.netlightedgeconference.network.APIHandler;
import conference.edge.netlight.netlightedgeconference.network.interfaces.FormListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.ProfileListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.SearchListener;

/**
 * Created by markus on 16-05-08.
 */
public class FormsFragment extends Fragment {

    private View mRoot;
    private RecyclerView mRecyclerView;
    private List<Form> mElements;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_forms, container, false);
        initViews();
        fetchData();
        return mRoot;
    }

    private void fetchData() {

        APIHandler.getInstance().getForms("Hejsan", new FormListener() {
            @Override
            public void onSuccess(List<Form> forms) {
                mElements = forms;
                mRecyclerView.setAdapter(new FormsAdapter(mElements));
            }

            @Override
            public void onFailure(int code) {

            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) mRoot.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}

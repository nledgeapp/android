package conference.edge.netlight.netlightedgeconference.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by markus on 16-05-08.
 */
public class User {
    private String name;
    private String mail;
    private List<Attribute> attributes;

    public User (String name, String mail, List<Attribute> attributes) {
        this.name = name;
        this.mail = mail;
        this.attributes = attributes;
    }


    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }
}

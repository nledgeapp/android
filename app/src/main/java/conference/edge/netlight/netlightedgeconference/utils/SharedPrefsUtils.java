package conference.edge.netlight.netlightedgeconference.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by markus on 16-04-11.
 */
public class SharedPrefsUtils {
    public static final String SHARED_PREFERENCES_NAME = "netlightSharedPrefs";

    private static SharedPrefsUtils sharedInstance;
    private SharedPreferences appSharedPreferences;

    private SharedPrefsUtils(Context context) {
        appSharedPreferences = context.getSharedPreferences(
                SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    public static SharedPrefsUtils sharedInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new SharedPrefsUtils(context);
        }

        return sharedInstance;
    }

    public String getPreference(
            final String name,
            final String defaultValue) {

        return appSharedPreferences.getString(name, defaultValue);
    }

    @SuppressLint("CommitPrefEdits")
    public void setPreference(
            final String name,
            final String value,
            final Boolean immediate) {

        SharedPreferences.Editor prefEditor = appSharedPreferences.edit();

        prefEditor.putString(name, value);

        if (immediate) {
            prefEditor.commit();
        } else {
            prefEditor.apply();
        }
    }

    public void clearPreference(
            final String name,
            final boolean immediate) {

        setPreference(name, "", immediate);
    }

    @SuppressLint("CommitPrefEdits")
    public void clearAllPreferences(final boolean immediate) {
        SharedPreferences.Editor prefEditor = appSharedPreferences.edit();

        prefEditor.clear();

        if (immediate) {
            prefEditor.commit();
        } else {
            prefEditor.apply();
        }
    }

    public int getPreference(
            final String name,
            final int defaultValue) {

        return appSharedPreferences.getInt(name, defaultValue);
    }

    @SuppressLint("CommitPrefEdits")
    public void setPreference(
            final String key,
            final int value,
            final Boolean immediate) {

        final SharedPreferences.Editor prefEditor = appSharedPreferences.edit();

        prefEditor.putInt(key, value);

        if (immediate) {
            prefEditor.commit();
        } else {
            prefEditor.apply();
        }
    }

    public Boolean getPreference(
            final String key,
            final Boolean defaultValue) {

        return appSharedPreferences.getBoolean(key, defaultValue);
    }

    @SuppressLint("CommitPrefEdits")
    public void setPreference(
            final String key,
            final Boolean value,
            final Boolean immediate) {

        final SharedPreferences.Editor prefEditor = appSharedPreferences.edit();

        prefEditor.putBoolean(key, value);

        if (immediate) {
            prefEditor.commit();
        } else {
            prefEditor.apply();
        }
    }

    public float getPreference(
            final String key,
            final float defaultValue) {

        return appSharedPreferences.getFloat(key, defaultValue);
    }

    @SuppressLint("CommitPrefEdits")
    public void setPreference(
            final String key,
            final float value,
            final Boolean immediate) {

        final SharedPreferences.Editor prefEditor = appSharedPreferences.edit();

        prefEditor.putFloat(key, value);

        if (immediate) {
            prefEditor.commit();
        } else {
            prefEditor.apply();
        }
    }

}
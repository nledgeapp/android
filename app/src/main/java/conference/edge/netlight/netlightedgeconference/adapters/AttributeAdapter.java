package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.models.AttributeElement;

/**
 * Created by markus on 16-05-16.
 */
public class AttributeAdapter extends RecyclerView.Adapter<AttributeAdapter.AttributeHolder> {

    private final List<AttributeElement> mElements;

    public AttributeAdapter(List<AttributeElement> elements) {
        mElements = elements;
    }

    @Override
    public AttributeHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_activity_attribute, parent, false);

        AttributeHolder holder = new AttributeHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(AttributeHolder holder, int position) {

        holder.name.setText(mElements.get(position).getName());
        holder.attributes.removeAllViews();

        for (int i = 0; i < mElements.get(position).getAttributes().size(); i++) {
            View view = LayoutInflater.from(holder.root.getContext()).inflate(R.layout.attribute_text, null);
            TextView tv = (TextView) view.findViewById(R.id.text);
            tv.setText(mElements.get(position).getAttributes().get(i).name);
            holder.attributes.addView(view);
        }

    }

    @Override
    public int getItemCount() {
        return mElements.size();
    }

    public class AttributeHolder extends RecyclerView.ViewHolder {

        public final View root;
        public final TextView name;
        public final LinearLayout attributes;

        public AttributeHolder(View itemView) {
            super(itemView);
            root = itemView;
            name = (TextView) root.findViewById(R.id.name);
            attributes = (LinearLayout) root.findViewById(R.id.attributes_container);

        }
    }
}
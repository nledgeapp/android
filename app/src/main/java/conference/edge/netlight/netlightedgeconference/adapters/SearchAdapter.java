package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.models.Person;

/**
 * Created by markus on 16-05-08.
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {

    private final List<Person> mElements;

    public SearchAdapter(List<Person> elements) {
        mElements = elements;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_search, parent, false);

        SearchHolder holder = new SearchHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, int position) {

        holder.name.setText(mElements.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mElements.size();
    }

    public class SearchHolder extends RecyclerView.ViewHolder {

        public final View root;
        public final TextView name;

        public SearchHolder(View itemView) {
            super(itemView);
            root = itemView;
            name = (TextView) root.findViewById(R.id.name);

        }
    }
}

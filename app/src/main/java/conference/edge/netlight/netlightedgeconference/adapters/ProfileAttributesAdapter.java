package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.listeners.AttributeListener;
import conference.edge.netlight.netlightedgeconference.models.Attribute;

/**
 * Created by markus on 16-05-08.
 */
public class ProfileAttributesAdapter extends RecyclerView.Adapter<ProfileAttributesAdapter.ViewHolder> {
    private final List<Attribute> mAttributes;
    private AttributeListener mListener;

    public ProfileAttributesAdapter(List<Attribute> attributes, AttributeListener listener) {
        this.mAttributes = attributes;
        mListener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attribute, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final int pos = position * 2;
        if (mAttributes.size() > pos + 1) {
            holder.rightName.setText(mAttributes.get(pos + 1).name);
            holder.rightType.setText(mAttributes.get(pos + 1).type);
            holder.rightDesc.setText(mAttributes.get(pos + 1).description);
            holder.rightSource.setText(mAttributes.get(pos + 1).source);
            holder.right.setVisibility(View.VISIBLE);
        } else {
            holder.right.setVisibility(View.INVISIBLE);
        }
        holder.leftName.setText(mAttributes.get(pos).name);
        holder.leftType.setText(mAttributes.get(pos).type);
        holder.leftDesc.setText(mAttributes.get(pos).description);
        holder.leftSource.setText(mAttributes.get(pos).source);


        holder.left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttribute(mAttributes.get(pos), holder.left);
            }
        });

        holder.right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttribute(mAttributes.get(pos + 1), holder.right);
            }
        });


    }

    private void showAttribute(Attribute attribute, View view) {
        mListener.showAttribute(attribute, view);
    }

    @Override
    public int getItemCount() {
        return (mAttributes.size() + 1) / 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public final View mRoot;
        public final TextView leftName;
        public final TextView leftType;
        public final TextView leftSource;
        public final TextView leftDesc;

        public final TextView rightName;
        public final TextView rightType;
        public final TextView rightSource;
        public final TextView rightDesc;
        public final View right;
        public final View left;

        public ViewHolder(View itemView) {
            super(itemView);
            mRoot = itemView;
            left = mRoot.findViewById(R.id.left);
            right = mRoot.findViewById(R.id.right);

            leftName = (TextView) left.findViewById(R.id.name);
            leftType = (TextView) left.findViewById(R.id.type);
            leftSource = (TextView) left.findViewById(R.id.source);
            leftDesc = (TextView) left.findViewById(R.id.desc);

            rightName = (TextView) right.findViewById(R.id.name);
            rightType = (TextView) right.findViewById(R.id.type);
            rightSource = (TextView) right.findViewById(R.id.source);
            rightDesc = (TextView) right.findViewById(R.id.desc);
        }
    }
}

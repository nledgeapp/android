package conference.edge.netlight.netlightedgeconference.models;

import java.util.List;

/**
 * Created by markus on 2016-10-24.
 */

public class Form {
    private List<FormAttribute> attributes;
    private String name;

    public String getName() {
        return name;
    }

    public List<FormAttribute> getAttributes() {
        return attributes;
    }

    public String getPercentageFinished() {
        double i = 0;
        for (FormAttribute attribute : attributes) {
            if (attribute.getRating() != -1) {
                i += 1;
            }
        }
        if (i == 0) {
            return "0%";
        }
        return String.format("%d %%", (int) (Math.round(i / attributes.size() * 100f)));
    }
}

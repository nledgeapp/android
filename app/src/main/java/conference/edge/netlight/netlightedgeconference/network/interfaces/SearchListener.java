package conference.edge.netlight.netlightedgeconference.network.interfaces;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.models.Person;

/**
 * Created by markus on 16-08-09.
 */
public interface SearchListener {
    void onSuccess(List<Person> persons);

    void onFailure(int code);

    void onFailure(String error);
}

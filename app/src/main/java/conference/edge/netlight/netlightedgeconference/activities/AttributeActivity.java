package conference.edge.netlight.netlightedgeconference.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.adapters.AttributeAdapter;
import conference.edge.netlight.netlightedgeconference.models.Attribute;
import conference.edge.netlight.netlightedgeconference.models.AttributeElement;
import conference.edge.netlight.netlightedgeconference.network.APIHandler;
import conference.edge.netlight.netlightedgeconference.network.interfaces.AttributeMatchesListener;

/**
 * Created by markus on 16-05-08.
 */
public class AttributeActivity extends AppCompatActivity {
    public static final String ARG_ATTRIBUTE = "argAttribute";
    private Attribute mAttribute;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private List<AttributeElement> mElements;
    private WebView mNextWebview;
    private List<WebView> webviews = new ArrayList<>();
    private int webviewPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attribute);
        initViews();
        getIntentData();
        setupToolbar();
        fetchData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //fixTransitionAnimation();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void fixTransitionAnimation() {
        postponeEnterTransition();

        final View decor = getWindow().getDecorView();
        decor.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onPreDraw() {
                decor.getViewTreeObserver().removeOnPreDrawListener(this);
                startPostponedEnterTransition();
                return true;
            }
        });
    }

    private void fetchData() {

        APIHandler.getInstance().getAttributeMatches("Markus", "empty", "?", new AttributeMatchesListener() {
            @Override
            public void onSuccess(List<AttributeElement> matches) {
                mElements = matches;
                mRecyclerView.setAdapter(new AttributeAdapter(mElements));
            }

            @Override
            public void onFailure(int code) {

            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        initEasterEgg();
    }

    private void initEasterEgg() {
        mNextWebview = (WebView) findViewById(R.id.horse_webview);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        final int width = displaymetrics.widthPixels;

        Resources r = getResources();
        final float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, r.getDisplayMetrics());

        mToolbar.findViewById(R.id.toolbar_title).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final WebView current = webviews.get(webviewPosition);
                webviewPosition++;
                current.setVisibility(View.VISIBLE);
                current.setBackgroundColor(Color.TRANSPARENT);
                current.setX(-px);
                int nextSpeed = new Random().nextInt(11000);
                current.animate().setInterpolator(new LinearInterpolator()).translationXBy(width + px).setDuration(50 + nextSpeed).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ((RelativeLayout) findViewById(R.id.content)).removeView(current);
                        current.destroy();
                        super.onAnimationEnd(animation);
                    }
                }).start();

                WebView webView = new WebView(AttributeActivity.this);
                webviews.add(webView);
                ((RelativeLayout) findViewById(R.id.content)).addView(webView);
                webView.setVisibility(View.GONE);
                ((RelativeLayout.LayoutParams) webView.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                webView.loadUrl("http://orig02.deviantart.net/d555/f/2014/202/1/2/running_zitox_by_journeyhorse-d7rnp1w.gif");
            }
        });
        webviews.add(mNextWebview);
        mNextWebview.setVisibility(View.GONE);
        mNextWebview.loadUrl("http://orig02.deviantart.net/d555/f/2014/202/1/2/running_zitox_by_journeyhorse-d7rnp1w.gif");

    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((TextView) mToolbar.findViewById(R.id.toolbar_title)).setText(mAttribute.name);
    }

    public void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            mAttribute = (Attribute) intent.getSerializableExtra(ARG_ATTRIBUTE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webviews.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out_to_right);
    }
}

package conference.edge.netlight.netlightedgeconference.network.interfaces;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.models.AttributeMatch;

/**
 * Created by markus on 16-08-09.
 */
public interface MatchesListener {

    void onSuccess(List<AttributeMatch> matches);

    void onFailure(int code);

    void onFailure(String error);
}

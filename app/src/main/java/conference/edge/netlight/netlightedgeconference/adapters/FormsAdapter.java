package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.models.Form;

/**
 * Created by markus on 2016-10-24.
 */

public class FormsAdapter extends RecyclerView.Adapter<FormsAdapter.FormHolder> {

    private final List<Form> forms;

    public FormsAdapter(List<Form> elements) {
        forms = elements;
    }

    @Override
    public FormsAdapter.FormHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_form, parent, false);

        FormsAdapter.FormHolder holder = new FormsAdapter.FormHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(FormsAdapter.FormHolder holder, int position) {
        holder.name.setText(forms.get(position).getName());
        holder.percentageFinished.setText(forms.get(position).getPercentageFinished());
    }

    @Override
    public int getItemCount() {
        return forms.size();
    }

    public class FormHolder extends RecyclerView.ViewHolder {

        public final TextView name;
        private final TextView percentageFinished;

        public FormHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            percentageFinished = (TextView) itemView.findViewById(R.id.percentage_finished);
        }
    }
}

package conference.edge.netlight.netlightedgeconference.models;

/**
 * Created by markus on 2016-10-24.
 */
public class FormAttribute {
    private String name;
    private int rating;

    public String getName() {
        return name;
    }

    public int getRating() {
        return rating;
    }
}

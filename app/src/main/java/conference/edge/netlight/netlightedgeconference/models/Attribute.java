package conference.edge.netlight.netlightedgeconference.models;

import java.io.Serializable;

/**
 * Created by markus on 16-05-08.
 */
public class Attribute implements Serializable{
    public int id;
    public String type;
    public String source;
    public String name;
    public String description;

    public Attribute(int id, String type, String source, String name, String description) {
        this.id = id;
        this.type = type;
        this.source = source;
        this.name = name;
        this.description = description;
    }
}

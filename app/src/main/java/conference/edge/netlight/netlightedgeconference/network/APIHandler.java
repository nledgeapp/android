package conference.edge.netlight.netlightedgeconference.network;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.Constants;
import conference.edge.netlight.netlightedgeconference.models.AttributeElement;
import conference.edge.netlight.netlightedgeconference.models.AttributeMatch;
import conference.edge.netlight.netlightedgeconference.models.Form;
import conference.edge.netlight.netlightedgeconference.models.Person;
import conference.edge.netlight.netlightedgeconference.models.User;
import conference.edge.netlight.netlightedgeconference.network.interfaces.AttributeMatchesListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.FormListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.MatchesListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.ProfileListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.SearchListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by markus on 16-08-09.
 */
public class APIHandler {
    private static final String BASE_URL = Constants.API_ENDPOINT;
    private API api;
    private static APIHandler sharedInstance;

    private APIHandler() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(API.class);
    }

    public static APIHandler getInstance() {
        if (sharedInstance == null) {
            synchronized (APIHandler.class) {
                if (sharedInstance == null) {
                    sharedInstance = new APIHandler();
                }
            }
        }
        return sharedInstance;
    }

    public void getProfile(String username, final ProfileListener listener) {
        Call<User> call = api.getProfile(username);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(response.code());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                listener.onFailure(call.request().toString());
            }
        });
    }

    public void searchPersons(String searchString, final SearchListener listener) {
        Call<List<Person>> call = api.searchPersons(searchString);
        call.enqueue(new Callback<List<Person>>() {
            @Override
            public void onResponse(Call<List<Person>> call, Response<List<Person>> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Person>> call, Throwable t) {
                listener.onFailure(call.request().toString());
            }
        });
    }

    public void getAttributeMatches(String username, String token, String attribute, final AttributeMatchesListener listener) {

        Call<List<AttributeElement>> call = api.getAttributeMatches(username, attribute, token);
        call.enqueue(new Callback<List<AttributeElement>>() {
            @Override
            public void onResponse(Call<List<AttributeElement>> call, Response<List<AttributeElement>> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(response.code());
                }
            }

            @Override
            public void onFailure(Call<List<AttributeElement>> call, Throwable t) {
                listener.onFailure(call.request().toString());
            }
        });

    }

    public void getMatches(String username, final MatchesListener listener) {

        Call<List<AttributeMatch>> call = api.getMatches(username);
        call.enqueue(new Callback<List<AttributeMatch>>() {
            @Override
            public void onResponse(Call<List<AttributeMatch>> call, Response<List<AttributeMatch>> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(response.code());
                }
            }

            @Override
            public void onFailure(Call<List<AttributeMatch>> call, Throwable t) {
                listener.onFailure(call.request().toString());
            }
        });

    }

    public void getForms(String username, final FormListener listener) {

        Call<List<Form>> call = api.getForms(username);
        call.enqueue(new Callback<List<Form>>() {
            @Override
            public void onResponse(Call<List<Form>> call, Response<List<Form>> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Form>> call, Throwable t) {
                listener.onFailure(call.request().toString());
            }
        });

    }
}

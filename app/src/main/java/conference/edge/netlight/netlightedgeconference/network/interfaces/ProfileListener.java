package conference.edge.netlight.netlightedgeconference.network.interfaces;

import conference.edge.netlight.netlightedgeconference.models.User;

/**
 * Created by markus on 16-08-09.
 */
public interface ProfileListener {
    void onSuccess(User user);

    void onFailure(int code);

    void onFailure(String error);
}

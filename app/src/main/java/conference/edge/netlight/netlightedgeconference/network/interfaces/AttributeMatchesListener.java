package conference.edge.netlight.netlightedgeconference.network.interfaces;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.models.AttributeElement;

/**
 * Created by markus on 16-08-09.
 */
public interface AttributeMatchesListener {
    void onSuccess(List<AttributeElement> matches);

    void onFailure(int code);

    void onFailure(String error);
}

package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import conference.edge.netlight.netlightedgeconference.fragments.FormsFragment;
import conference.edge.netlight.netlightedgeconference.fragments.MatchesFragment;
import conference.edge.netlight.netlightedgeconference.fragments.ProfileFragment;
import conference.edge.netlight.netlightedgeconference.fragments.SearchFragment;

/**
 * Created by markus on 16-05-08.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new ProfileFragment();
                break;
            case 1:
                fragment = new FormsFragment();
                break;
            case 2:
                fragment = new MatchesFragment();
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Profile";
            case 1:
                return "Forms";
            case 2:
                return "Matches";
        }

        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}

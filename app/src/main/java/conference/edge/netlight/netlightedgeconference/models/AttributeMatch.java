package conference.edge.netlight.netlightedgeconference.models;

import java.util.List;

/**
 * Created by markus on 16-08-09.
 */
public class AttributeMatch {
    private String attribute;
    private List<Person> persons;

    public List<Person> getPersons() {
        return persons;
    }

    public String getAttribute() {
        return attribute;
    }
}

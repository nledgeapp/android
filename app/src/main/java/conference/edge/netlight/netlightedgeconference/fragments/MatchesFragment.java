package conference.edge.netlight.netlightedgeconference.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.flingswipe.SwipeFlingAdapterView;
import conference.edge.netlight.netlightedgeconference.models.AttributeMatch;
import conference.edge.netlight.netlightedgeconference.models.MatchesElement;
import conference.edge.netlight.netlightedgeconference.models.Person;
import conference.edge.netlight.netlightedgeconference.network.APIHandler;
import conference.edge.netlight.netlightedgeconference.network.interfaces.MatchesListener;

/**
 * Created by markus on 16-05-16.
 */
public class MatchesFragment extends Fragment {

    private View mRoot;
    private LinearLayout mFLingCardContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_matches, container, false);
        initViews();
        //setData();
        fetchData();
        return mRoot;
    }

    private void fetchData() {
        APIHandler.getInstance().getMatches("random", new MatchesListener() {
            @Override
            public void onSuccess(List<AttributeMatch> matches) {
                updateMatches(matches);
            }

            @Override
            public void onFailure(int code) {

            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

    private void updateMatches(List<AttributeMatch> matches) {
        mFLingCardContainer.removeAllViews();

        for (AttributeMatch match : matches) {

            final View view = LayoutInflater.from(getContext()).inflate(R.layout.match_container, null);
            final SwipeFlingAdapterView flingCard = (SwipeFlingAdapterView) view.findViewById(R.id.fling_card);

            final List<String> personList = new ArrayList<>();
            for (Person person : match.getPersons()) {
                personList.add(person.getName());
            }

            flingCard.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.row_matches, R.id.name, personList));

            flingCard.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                @Override
                public void removeFirstObjectInAdapter() {
                    personList.remove(0);
                    ((ArrayAdapter) flingCard.getAdapter()).notifyDataSetChanged();
                }

                @Override
                public void onLeftCardExit(Object dataObject) {

                }

                @Override
                public void onRightCardExit(Object dataObject) {

                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                    personList.add("XML ".concat(String.valueOf(0)));
                    ((ArrayAdapter) flingCard.getAdapter()).notifyDataSetChanged();

                }

                @Override
                public void onScroll(float scrollProgressPercent) {

                }
            });

            ((TextView) view.findViewById(R.id.text_flag)).setText(match.getAttribute());
            mFLingCardContainer.addView(view);

        }

    }

    private void initViews() {
        mFLingCardContainer = (LinearLayout) mRoot.findViewById(R.id.container_fling_cards);
    }
}

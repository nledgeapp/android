package conference.edge.netlight.netlightedgeconference.network;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.models.AttributeElement;
import conference.edge.netlight.netlightedgeconference.models.AttributeMatch;
import conference.edge.netlight.netlightedgeconference.models.Form;
import conference.edge.netlight.netlightedgeconference.models.Person;
import conference.edge.netlight.netlightedgeconference.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by markus on 16-08-09.
 */
public interface API {

    @GET("/attributeMatches")
    Call<List<AttributeElement>> getAttributeMatches(@Query("username") String username, @Query("attribute") String attribute, @Query("token") String token);

    @GET("/profile")
    Call<User> getProfile(@Query("username") String username);

    @GET("/search")
    Call<List<Person>> searchPersons(@Query("searchString") String searchString);

    @GET("/matches")
    Call<List<AttributeMatch>> getMatches(@Query("username") String username);

    @GET("/questions")
    Call<List<Form>> getForms(@Query("username") String username);
}

package conference.edge.netlight.netlightedgeconference.models;

/**
 * Created by markus on 16-05-16.
 */
public class MatchesElement {
    private String name;

    public MatchesElement(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

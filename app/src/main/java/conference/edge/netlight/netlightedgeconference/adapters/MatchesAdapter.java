package conference.edge.netlight.netlightedgeconference.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.models.MatchesElement;

/**
 * Created by markus on 16-05-16.
 */
public class MatchesAdapter extends RecyclerView.Adapter<MatchesAdapter.MatchesHolder> {

    private final List<MatchesElement> mElements;

    public MatchesAdapter(List<MatchesElement> elements) {
        mElements = elements;
    }

    @Override
    public MatchesHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_matches, parent, false);

        MatchesHolder holder = new MatchesHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MatchesHolder holder, int position) {

        holder.name.setText(mElements.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mElements.size();
    }

    public class MatchesHolder extends RecyclerView.ViewHolder {

        public final View root;
        public final TextView name;

        public MatchesHolder(View itemView) {
            super(itemView);
            root = itemView;
            name = (TextView) root.findViewById(R.id.name);

        }
    }
}
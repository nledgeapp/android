package conference.edge.netlight.netlightedgeconference.models;

/**
 * Created by markus on 16-05-08.
 */
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package conference.edge.netlight.netlightedgeconference.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import conference.edge.netlight.netlightedgeconference.R;
import conference.edge.netlight.netlightedgeconference.activities.AttributeActivity;
import conference.edge.netlight.netlightedgeconference.adapters.AttributeAdapter;
import conference.edge.netlight.netlightedgeconference.adapters.ProfileAttributesAdapter;
import conference.edge.netlight.netlightedgeconference.listeners.AttributeListener;
import conference.edge.netlight.netlightedgeconference.models.Attribute;
import conference.edge.netlight.netlightedgeconference.models.AttributeElement;
import conference.edge.netlight.netlightedgeconference.models.User;
import conference.edge.netlight.netlightedgeconference.network.APIHandler;
import conference.edge.netlight.netlightedgeconference.network.interfaces.AttributeMatchesListener;
import conference.edge.netlight.netlightedgeconference.network.interfaces.ProfileListener;

/**
 * Created by markus on 16-05-08.
 */
public class ProfileFragment extends Fragment implements AttributeListener {
    private ImageView mProfilePicture;
    private TextView mUserEmail;
    private TextView mUserName;
    private View mRoot;
    private User mUser;
    private RecyclerView mAttributes;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_profile, container, false);
        initViews();
        fetchData();
        return mRoot;
    }

    private void fetchData() {

        APIHandler.getInstance().getProfile("Markus", new ProfileListener() {
            @Override
            public void onSuccess(User user) {
                mUser = user;
                updateLayout();
            }

            @Override
            public void onFailure(int code) {

            }

            @Override
            public void onFailure(String error) {

            }
        });
    }

    private void setFakeData() {
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute(0, "Skill", "linkedin", "Java", "Expert"));
        attributes.add(new Attribute(1, "Skill", "linkedin", "C++", "Basic"));
        attributes.add(new Attribute(2, "Skill", "linkedin", "Python", "Expert"));
        attributes.add(new Attribute(3, "Skill", "facebook", "Sketch", "Expert"));
        attributes.add(new Attribute(4, "Skill", "facebook", "UX", "Noobie"));
        attributes.add(new Attribute(5, "Skill", "linkedin", "Haskell", "Senior"));

        mUser = new User("Markus", "Markus.larsson@netlight.com", attributes);
        updateLayout();
    }

    private void updateLayout() {
        mUserName.setText(mUser.getName());
        mUserEmail.setText(mUser.getMail());
        mAttributes.setAdapter(new ProfileAttributesAdapter(mUser.getAttributes(), this));

    }


    private void initViews() {
        mProfilePicture = (ImageView) mRoot.findViewById(R.id.profile_picture);
        mUserEmail = (TextView) mRoot.findViewById(R.id.user_email);
        mUserName = (TextView) mRoot.findViewById(R.id.user_name);
        mAttributes = (RecyclerView) mRoot.findViewById(R.id.attributes);
        mAttributes.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void showAttribute(Attribute attribute, View view) {
        Intent intent = new Intent(getActivity(), AttributeActivity.class);
        intent.putExtra(AttributeActivity.ARG_ATTRIBUTE, attribute);

        if (false && Build.VERSION.SDK_INT >= 21) {

            View navBar = getActivity().findViewById(android.R.id.navigationBarBackground);
            Pair<View, String> list[] = new Pair[navBar == null ? 3 : 4];
            list[0] = new Pair<>(view.findViewById(R.id.name),
                    getString(R.string.transition_attribute));
            list[1] = new Pair<>(getActivity().findViewById(R.id.toolbar), getString(R.string.transition_appbar));
            list[2] = new Pair<>(getActivity().findViewById(android.R.id.statusBarBackground), Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME);
            if (navBar != null) {
                list[3] = new Pair<>(navBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME);
            }

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    // the context of the activity
                    getActivity(), list);

            ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
        } else {
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_out);
        }

    }
}

package conference.edge.netlight.netlightedgeconference.network.interfaces;

import java.util.List;

import conference.edge.netlight.netlightedgeconference.models.Form;


/**
 * Created by markus on 2016-10-24.
 */

public interface FormListener {
    void onSuccess(List<Form> forms);

    void onFailure(int code);

    void onFailure(String error);
}

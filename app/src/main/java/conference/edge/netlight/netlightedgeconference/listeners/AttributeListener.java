package conference.edge.netlight.netlightedgeconference.listeners;

import android.view.View;

import conference.edge.netlight.netlightedgeconference.models.Attribute;

/**
 * Created by markus on 16-05-08.
 */
public interface AttributeListener {
    void showAttribute(Attribute attribute, View view);
}
